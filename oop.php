<?php

class BITM
{
    public $window;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;

    public function coolTheAir()
    {
        echo "I am cooling the air";
    }


    public function compute()
    {
        echo "I am computing";
    }


    public function show()
    {

        echo $this->window."<br>";
        echo $this->door."<br>";
        echo $this->table."<br>";
        echo $this->chair."<br>";
        echo $this->whiteboard."<br>";

    }


    public function setAll(){
        $this->setChair("I am a chair");
        $this->setTable("I am a table");
        $this->setDoor("I am a door");
        $this->setWindow("I am a window");
        $this->setWhiteboard("I am a whiteboard");

    }



    public function setWindow($window)
    {
        $this->window = $window;

    }

    public function setDoor($door)
    {
        $this->door = $door;

    }

    public function setChair($chair)
{
    $this->chair = $chair;

}

    public function setTable($table)
    {
        $this->table = $table;

    }

    public function setWhiteboard($whiteboard)
    {
        $this->whiteboard = $whiteboard;

    }
//END OF CLASS BITM
}

$obj_BITM_at_ctg=new BITM;

$obj_BITM_at_ctg->setChair("I am a chair");
$obj_BITM_at_ctg->setTable("I am a table");
$obj_BITM_at_ctg->setDoor("I am a door");
$obj_BITM_at_ctg->setWindow("I am a window");
$obj_BITM_at_ctg->setWhiteboard("I am a whiteboard");
$obj_BITM_at_ctg->show();


class BITM_LAB402 extends BITM{

}
/* $objBITM_LAB=new BITM_LAB402();
$objBITM_LAB->setALL();
$obj_BITM_at_ctg->show();*/


$objBITM_LAB=new BITM_LAB402();
$obj_BITM_at_ctg->setChair("This is a chair");
$obj_BITM_at_ctg->setTable("This is a table");
$obj_BITM_at_ctg->setDoor("This  a door");
$obj_BITM_at_ctg->setWindow("This a window");
$obj_BITM_at_ctg->setWhiteboard("This a whiteboard");
$obj_BITM_at_ctg->show();

